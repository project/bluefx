<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body>

<table cellpadding="0" cellspacing="0" width="800" align="center" border="0">
	<tr>
		<td width="194">
		<?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
		</td>
		<td align="center">
		<?php print $search_box ?>
		</td>
	</tr>
</table>
<div style="height: 1px; background-color: #5a94cc;"></div>
<table border="0" cellpadding="0" cellspacing="0" width="800" align="center">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<table cellpadding="0" cellspacing="0" width="428" align="center">
	<tr>
		<td align="center" width="428" height="29" background="themes/bluefx/footer.png" style="font-size:10px; ">
		<?php print $footer_message ?></td>
	</tr>
	<tr>
		<td height="10">&nbsp;</td>
	</tr>
</table>
<?php print $closure ?>
</body>
</html>